package cn.itcast.user.web;

import cn.itcast.user.config.Pattern;
import cn.itcast.user.pojo.User;
import cn.itcast.user.service.UserService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@RestController
@RequestMapping("/user")
//@RefreshScope
public class UserController {

    @Autowired
    private UserService userService;
//    @Value("${pattern.dateformat}")


    /**
     * 路径： /user/110
     *
     * @param id 用户id
     * @return 用户
     */
    @GetMapping("/{id}")
    public User queryById(@PathVariable("id") Long id) {
        return userService.queryById(id);
    }
    @Resource
    private Pattern pattern;
    @GetMapping("/now")
    public String now(){
        String now = LocalDateTime.now().
                format(DateTimeFormatter.ofPattern(pattern.getDateformat()));
        return now;
    }
    @GetMapping("/shape")
    public Pattern pattern(){
        return pattern;
    }
}
