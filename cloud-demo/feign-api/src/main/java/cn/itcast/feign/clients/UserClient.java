package cn.itcast.feign.clients;


import cn.itcast.feign.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 */
@FeignClient("userserver")
public interface UserClient {
//    String url="http://userserver/user/"+order.getUserId();
//    User user = restTemplate.getForObject(url, User.class);
    @GetMapping("/user/{userid}")
    public User findById(@PathVariable("userid") Long userid);
}
