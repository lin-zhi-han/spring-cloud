package cn.itcast.mq.config;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 *
 */
@Component
public class SpringRabbitListener {

    @RabbitListener(queues = "simple.queue")
    public void listener(String  msg){
        System.out.println("将接收到消息"+msg);
    }
    @RabbitListener(queues = "queue1")
    public void listenerWorkQueue1(String  msg) throws InterruptedException {
        System.out.println("消费者1接收到消息"+msg+ LocalDateTime.now());
        Thread.sleep(20);
    }

    @RabbitListener(queues = "queue1")
    public void listenerWorkQueue2(String  msg) throws InterruptedException {
        System.err.println("消费者2接收到消息"+msg+LocalDateTime.now());
        Thread.sleep(200);
    }
    @RabbitListener(queues = "fanout.queue1")
    public void listenerFanout1(String  msg) throws InterruptedException {
        System.err.println("fanout1接收消息"+msg+LocalDateTime.now());

    }
    @RabbitListener(queues = "fanout.queue2")
    public void listenerFanout2(String  msg) throws InterruptedException {
        System.err.println("fanout2接收消息"+msg+LocalDateTime.now());

    }
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(name = "direct.queue1"),
            exchange = @Exchange(name = "direct.exchange",type = ExchangeTypes.DIRECT),
            key = {"blue","yellow"}
    )})
    public void listenerDirectExchange1(String msg){
        System.out.println("消费者1消费"+msg);
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "direct.queue2"),
                                exchange = @Exchange(name = "direct.exchange",type = ExchangeTypes.DIRECT),
                                key = {"blue","red"}
    )})
    public void listenerDirectExchange2(String msg){
        System.out.println("消费者2消费"+msg);
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "topic.queue1"),
            exchange = @Exchange(name = "topic.exchange",type = ExchangeTypes.TOPIC),
            key = {"china.#"}
    )})
    public void listenerTopicExchange1(String msg){
        System.out.println("消费者1消费"+msg);
    }
    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "topic.queue2"),
            exchange = @Exchange(name = "topic.exchange",type = ExchangeTypes.TOPIC),
            key = {"#.news"}
    )})
    public void listenerTopicExchange2(String msg){
        System.out.println("消费者2消费"+msg);
    }


}
