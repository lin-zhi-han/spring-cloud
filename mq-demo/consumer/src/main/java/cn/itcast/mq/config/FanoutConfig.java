package cn.itcast.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class FanoutConfig {
    //交换机
    @Bean
    public FanoutExchange fanoutExchange(){

        return new FanoutExchange("fanout.exchange");
    }
    //队列1
    @Bean
    public Queue getQueue1(){
        return new Queue("fanout.queue1");
    }

    //队列2
    @Bean
    public Queue getQueue2(){
        return new Queue("fanout.queue2");
    }
    //绑定
    @Bean
    public Binding fanout1Exchange(FanoutExchange fanoutExchange,Queue getQueue1){
        return BindingBuilder.bind(getQueue1).to(fanoutExchange);
    }
    @Bean
    public Binding fanout2Exchange(FanoutExchange fanoutExchange,Queue getQueue2){
        return BindingBuilder.bind(getQueue2).to(fanoutExchange);
    }
}
