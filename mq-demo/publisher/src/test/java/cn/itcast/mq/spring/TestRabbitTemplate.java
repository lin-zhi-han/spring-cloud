package cn.itcast.mq.spring;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 */

@SpringBootTest
public class TestRabbitTemplate {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    public void test(){
        String queueName="simple.queue";
        String message="hello world";
        rabbitTemplate.convertAndSend(queueName,message);
        System.out.println(rabbitTemplate);
    }
    @Test
    public void testWorkQueue() throws InterruptedException {
        String queueName="queue1";
        String message="hello world__";

        for (int i = 1; i <=50; i++) {
            rabbitTemplate.convertAndSend(queueName,message+i);
            Thread.sleep(20);
        }
    }
    @Test
    public void testFanoutExchange() throws InterruptedException {
        String ExchangeName="fanout.exchange";
        String message="hello world__";
       rabbitTemplate.convertAndSend(ExchangeName,"",message);

    }
    @Test
    public void testDirectExchange() throws InterruptedException {
        String ExchangeName="direct.exchange";
        String message="hello world__";
        rabbitTemplate.convertAndSend(ExchangeName,"blue",message);

    }
    @Test
    public void testTopicExchange() throws InterruptedException {
        String ExchangeName="topic.exchange";
        String message="china";
        rabbitTemplate.convertAndSend(ExchangeName,"china.news",message);

    }

}
